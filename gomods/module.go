package gomods

import (
	"cmp"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqnode"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/dryad/ordset"
	"golang.org/x/mod/modfile"
)

// A Module which uses Go module pseudo-versions.
type Module struct {
	ModFile       *modfile.File
	MP            ModulePath
	PseudoVersion string
}

// Replaces reports whether the module has a replace directive pointing to the
// given module path.
func (m Module) Replaces(mp ModulePath) bool {
	for _, r := range m.ModFile.Replace {
		if r.Old.Path == string(mp) {
			return true
		}
	}
	return false
}

// RequiresVersion reports whether the receiver module's require directive for
// the given module path requires the given version of the module path.
//
// It is an error if the receiver module does not require the given module path.
func (m Module) RequiresVersion(mp ModulePath, ver string) (bool, error) {
	for _, r := range m.ModFile.Require {
		if r.Indirect {
			continue
		}
		if r.Mod.Path == string(mp) {
			return r.Mod.Version == ver, nil
		}
	}
	return false,
		fmt.Errorf("no such direct require in module %v for %v", m.MP, mp)
}

// A ModulePath is the canonical name of a module.
//
// https://go.dev/ref/mod#module-path
type ModulePath string

// BaseDir is the name of the directory with the module's go.mod file.
func (mp ModulePath) BaseDir() string {
	return filepath.Base(mp.Dir())
}

// Dir is the directory path of the module's go.mod file.
func (mp ModulePath) Dir() string {
	return filepath.Dir(mp.FilePath())
}

// FilePath of the module's go.mod file relative to the current directory which
// is assumed to be a sibling directory of the module's directory.
func (mp ModulePath) FilePath() string {
	return filepath.Join("../", filepath.Base(string(mp)), "go.mod")
}

// Parse the module's Go module file.
//
// go.mod is the standard filename of a Go module file.
func (mp ModulePath) Parse() (*modfile.File, error) {
	fp := mp.FilePath()
	b, err := os.ReadFile(fp)
	if err != nil {
		return nil, err
	}
	mod, err := modfile.Parse(fp, b, nil)
	if err != nil {
		return nil, err
	}
	return mod, nil
}

// PseudoVersion of the module of the form v0.0.0-yyyymmddhhmmss-abcdefabcdef
// freshly generated from the module's git commit data.
//
// https://go.dev/ref/mod#pseudo-versions
func (mp ModulePath) PseudoVersion() (string, error) {
	var (
		err                                      error
		hash, revisionIdentifier, timestamp, ver string
	)

	hash, err = LatestCommitHash(mp)
	if err != nil {
		return "", err
	}
	revisionIdentifier = hash[:12]

	timestamp, err = LatestCommitTime(mp)
	if err != nil {
		return "", err
	}

	ver = strings.Join([]string{"v0.0.0", timestamp, revisionIdentifier}, "-")
	return ver, err
}

func (mp ModulePath) String() string {
	return string(mp)
}

// NewModuleTree constructs an abstract module dependency tree whose root module
// is the module with the given module path.
//
// Modules that do not use the given repository are ignored.
//
// In a module dependency tree a parent module directly depends on its child
// modules which in turn may have their own direct dependencies. Note that any
// given module may occur many times in the tree.
//
// On disk the modules in question are expected to be siblings in a single
// directory hence the module tree is an abstract representation of their
// dependency relationships.
func NewModuleTree(
	repo string,
	rootMP ModulePath,
) (*dryad.NodeOfEq[Module], error) {

	repoPattern := regexp.MustCompile("^" +
		strings.Replace(repo, `.`, `\.`, 1))

	pseudoPattern := regexp.MustCompile("^" +
		strings.Replace("v0.0.0-", `.`, `\.`, 2))

	// To allow equality comparisons between modules make sure all the modfile
	// pointers are the same for the same module.
	modFiles := map[ModulePath]*modfile.File{}

	isOwnAncestor := func(anc *dryad.NodeOfEq[Module], mp ModulePath) bool {
		for anc != nil {
			if anc.Content.MP == mp {
				return true
			}
			anc = anc.Cashpoint
		}
		return false
	}

	var addDependencies func(node *dryad.NodeOfEq[Module]) error
	addDependencies = func(node *dryad.NodeOfEq[Module]) error {

		if ok, err := IsClean(node.Content.MP); !ok || err != nil {
			if !ok {
				return fmt.Errorf("unclean git repository: %v", node.Content.MP)
			}
			return err
		}

		for _, r := range node.Content.ModFile.Require {
			if r.Indirect ||
				!repoPattern.Match([]byte(r.Mod.Path)) ||
				!pseudoPattern.Match([]byte(r.Mod.Version)) {
				continue
			}

			var (
				err     error
				modFile *modfile.File
				path    = ModulePath(r.Mod.Path)
				ok      bool
				ver     string
			)

			if isOwnAncestor(node, path) {
				return fmt.Errorf("module dependency cycle: %v", path)
			}

			if modFile, ok = modFiles[path]; !ok {
				modFile, err = path.Parse()
				modFiles[path] = modFile
				if err != nil {
					return err
				}
			}

			ver, err = path.PseudoVersion()
			if err != nil {
				return err
			}

			moduleNode := &dryad.NodeOfEq[Module]{
				Content: Module{
					ModFile:       modFile,
					MP:            path,
					PseudoVersion: ver,
				},
			}

			node.AddChild(moduleNode)
		}

		for _, child := range node.Kids {
			if err := addDependencies(child); err != nil {
				return err
			}
		}

		return nil
	}

	rootModFile, err := rootMP.Parse()
	if err != nil {
		return nil, err
	}
	rootNode := &dryad.NodeOfEq[Module]{
		Content: Module{
			ModFile:       rootModFile,
			MP:            rootMP,
			PseudoVersion: "", // Not required for root module.
		},
	}

	err = addDependencies(rootNode)

	return rootNode, err
}

func PrintModuleTree(depth int, parent *dryad.NodeOfEq[Module]) {
	var indent string
	for i := 0; i < depth; i++ {
		indent += "    "
	}
	fmt.Printf("%v%v\n", indent, parent.Content.MP)
	for _, k := range parent.Kids {
		PrintModuleTree(depth+1, k)
	}
}

// LeafMPs is a sorted slice of module paths without duplications that are the
// leaves on the tree with the given root.
func LeafMPs(root *dryad.NodeOfEq[Module]) []ModulePath {
	return ordset.Sorted(eqset.MapToOrd(
		eqnode.LeafSet(root),
		func(m Module) ModulePath { return m.MP },
	))
}

// SortedLeafUsers are the modules on the given tree that directly depend on the
// given leaf module.
//
// In the returned slice modules are sorted by name in ascending order and there
// are no duplicates.
func SortedLeafUsers(leafMP ModulePath, tree *dryad.NodeOfEq[Module]) []Module {
	leafUsers := dryad.Set[Module]{}
	for _, leafNode := range eqnode.Leaves(tree) {
		if leafNode.Content.MP == leafMP {
			leafUsers.Add(leafNode.Cashpoint.Content)
		}
	}
	return leafUsers.Sorted(
		func(m1, m2 Module) int { return cmp.Compare(m1.MP, m2.MP) },
	)
}

// ModFiles exist in the root directory of a Go module.
var ModFiles = dryad.Set[string]{
	"go.mod": struct{}{},
	"go.sum": struct{}{},
}

// AssertDirtyState is an error if any files other than the Go module files are
// changed.
//
// Else returns the changed files.
func AssertDirtyState(mp ModulePath) ([]string, error) {
	changed, err := ModifiedFileNames(mp)
	if err != nil {
		return nil, err
	}
	if dif := changed.Difference(ModFiles); len(dif) > 0 {
		return nil,
			fmt.Errorf("unexpected file modification: %v", dif.Elements())
	}
	return changed.Elements(), err
}
