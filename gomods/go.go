package gomods

import (
	"log"
	"os"
	"os/exec"
)

func GoGet(mp ModulePath) {
	cmd := exec.Command("go", "get", "-u", "./...")
	cmd.Dir = mp.Dir()
	cmd.Env = append(os.Environ(), "GOPROXY=direct")
	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
}

// GoModEditGo updates the Go compiler version.
//
// A typical Go version looks like:
// 1.18
func GoModEditGo(mp ModulePath, version string) {
	cmd := exec.Command("go", "mod", "edit", "-go", version)
	cmd.Dir = mp.Dir()
	cmd.Env = append(os.Environ(), "GOPROXY=direct")
	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
}

func GoModTidy(mp ModulePath) {
	cmd := exec.Command("go", "mod", "tidy")
	cmd.Dir = mp.Dir()
	cmd.Env = append(os.Environ(), "GOPROXY=direct")
	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
}
