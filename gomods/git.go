package gomods

import (
	"bufio"
	"fmt"
	"io"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/fospathi/dryad"
)

func CommitChanges(mp ModulePath, filenames []string) error {
	const commitMessage = "Update dependencies with fospathitool"
	if len(filenames) == 0 {
		return nil
	}
	dir := filepath.Dir(mp.FilePath())

	for _, fn := range filenames {
		cmd := exec.Command("git", "add", fn)
		cmd.Dir = dir
		if err := cmd.Run(); err != nil {
			return err
		}
	}

	cmd := exec.Command("git", "commit", "-m", commitMessage)
	cmd.Dir = dir
	return cmd.Run()
}

func IsClean(mp ModulePath) (bool, error) {
	// https://git-scm.com/docs/git-status/2.2.3#Documentation/git-status.txt---porcelain
	cmd := exec.Command("git", "status", "--porcelain")
	cmd.Dir = mp.Dir()
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return false, err
	}
	if err = cmd.Start(); err != nil {
		return false, err
	}
	output, err := io.ReadAll(stdout)
	if err != nil {
		return false, err
	}
	if err = cmd.Wait(); err != nil {
		return false, err
	}
	return len(output) == 0, nil
}

func LatestCommitHash(mp ModulePath) (string, error) {
	// https://git-scm.com/docs/git-log#Documentation/git-log.txt-emHem
	cmd := exec.Command("git", "log", "-1", "--format=%H")
	cmd.Dir = mp.Dir()
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "", err
	}
	if err = cmd.Start(); err != nil {
		return "", err
	}
	output, err := io.ReadAll(stdout)
	if err != nil {
		return "", err
	}
	if err = cmd.Wait(); err != nil {
		return "", err
	}
	hash := strings.TrimSpace(string(output))
	const hexFormatCommitHashLen = 40
	if len(hash) != hexFormatCommitHashLen {
		return "", fmt.Errorf("incorrect commit hash format: %v", hash)
	}
	return hash, nil
}

// LatestCommitTime using the same time format as used in Go pseudo-versions.
func LatestCommitTime(mp ModulePath) (string, error) {
	// https://git-scm.com/docs/git-log#Documentation/git-log.txt-emctem
	cmd := exec.Command("git", "log", "-1", "--format=%ct")
	cmd.Dir = mp.Dir()
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "", err
	}
	if err = cmd.Start(); err != nil {
		return "", err
	}
	output, err := io.ReadAll(stdout)
	if err != nil {
		return "", err
	}
	if err = cmd.Wait(); err != nil {
		return "", err
	}
	unixTime, err := strconv.ParseInt(strings.TrimSpace(string(output)), 10, 64)
	if err != nil {
		return "", err
	}
	// Times contained in Go pseudo-versions are UTC.
	t := time.Unix(unixTime, 0).UTC()
	return t.Format("20060102150405"), nil
}

func ModifiedFileNames(mp ModulePath) (dryad.Set[string], error) {
	// https://git-scm.com/docs/git-ls-files#Documentation/git-ls-files.txt--m
	cmd := exec.Command("git", "ls-files", "-m")
	cmd.Dir = mp.Dir()
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	if err := cmd.Start(); err != nil {
		return nil, err
	}
	var (
		modified = dryad.Set[string]{}
		scanner  = bufio.NewScanner(stdout)
	)
	for scanner.Scan() {
		modified.Add(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	if err := cmd.Wait(); err != nil {
		return nil, err
	}
	return modified, nil
}

func PushChanges(mp ModulePath) error {
	cmd := exec.Command("git", "push", "origin", "master")
	cmd.Dir = mp.Dir()
	return cmd.Run()
}
