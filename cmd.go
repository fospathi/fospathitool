package main

import (
	"flag"
	"fmt"
	"strings"
)

// Args contains either default values or, when given, values passed to the app
// as command-line flags.
type Args struct {
	// Set the modules to use this version of the Go compiler.
	GoVersion string
	// Modules are only affected by this tool if they have module path names
	// that share this common repository prefix.
	//
	// For example, in the module path "gitlab.com/fospathi/mechane", the
	// common repository prefix is gitlab.com/fospathi.
	//
	// https://golang.org/ref/mod#module-path
	ModRepo string
	// The name of the root module's directory, a module which shall be the root
	// of the dependency tree of modules.
	//
	// For example, in the module path "gitlab.com/fospathi/mechane", the
	// directory name is mechane.
	RootModDirName string
}

// ParseFlags given on the command-line.
func ParseFlags() (Args, error) {

	const (
		defaultGoVer = "1.21"
		defaultRepo  = "gitlab.com/fospathi/"
		defaultRoot  = "mechane"
	)

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of fospathitool:\n")
		flag.PrintDefaults()
	}

	goVer := flag.String(
		"go",
		defaultGoVer,
		"The modules are set to use this version of the Go compiler\n",
	)

	repo := flag.String(
		"repo",
		defaultRepo,
		"The repository component of the module path and by which modules are "+
			"\njudged as interrelated\n",
	)

	root := flag.String(
		"root",
		defaultRoot,
		"The directory name component of the module path of the root module\n",
	)

	flag.Parse()

	if !strings.HasSuffix(*repo, "/") {
		*repo += "/"
	}

	if len(flag.Args()) > 0 {
		return Args{}, fmt.Errorf("unexpected arguments: %v", flag.Args())
	}

	return Args{
		GoVersion:      *goVer,
		ModRepo:        *repo,
		RootModDirName: *root,
	}, nil
}
