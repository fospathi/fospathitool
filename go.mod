module gitlab.com/fospathi/fospathitool

go 1.21

require (
	gitlab.com/fospathi/dryad v0.0.0-20231105005730-60ed499ed55b
	gitlab.com/fospathi/universal v0.0.0-20230605055348-86065fb86769
	golang.org/x/mod v0.14.0
)

require (
	github.com/gorilla/websocket v1.5.1 // indirect
	golang.org/x/exp v0.0.0-20231006140011-7918f672742d // indirect
	golang.org/x/net v0.17.0 // indirect
)

replace gitlab.com/fospathi/dryad => ../dryad

replace gitlab.com/fospathi/universal => ../universal
