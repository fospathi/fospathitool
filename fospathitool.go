/*

Manage interrelated Go modules on the local disk.


Warning
=======

While the tool is running do not edit any modules on the local disk.


Conditions
==========

This is not a general tool, it only works on a certain subset of modules that
meet these conditions:

* Are interrelated: for modules to be considered as possibly interrelated it is
  required that their module path names have the same repository component and
  that their root directories are siblings in a single directory.

  https://go.dev/ref/mod#module-path

* Are siblings with fospathitool: their root directories are siblings with the
  root directory of this module.

* Use replace directives for all interrelated requires.

  https://go.dev/ref/mod#go-mod-file-replace

* Use git. Also they need to have a clean repository. Also they should be set up
  for non-interactive git pushes.

* Are untagged: thus they are assigned pseudo-versions automatically by Go.
  Specifically fospathitool uses v0.0.0-... type pseudo-versions.

  https://go.dev/ref/mod#pseudo-versions



Dependency tree
===============

In Go a module dependency graph like

        A
        |\
        | \
        |  B
        | /
        |/
        C

can be mapped to a tree like

        A
       / \
      C   B
           \
            C

A node's contents can be the same as another node's. If one node of a certain
value is a leaf node then all nodes with that value are leaf nodes. Thus we can
shave leaf nodes off the tree layer by layer until only the root node remains.

The correct update order for the modules is obtained by performing the following
steps in order:

1.) Update and tidy the set of modules comprising the tree's leaves and commit
any changes.

2.) If the root module is the only leaf remaining on the tree you are finished.

3.) Propagate the new module versions of changed modules to the set of modules
who are their direct users.

4.) Remove the leaf nodes from the tree.

5.) Go to step 1.)


*/
package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sync"

	"gitlab.com/fospathi/dryad/eqnode"
	"gitlab.com/fospathi/fospathitool/gomods"
	"gitlab.com/fospathi/universal/mech"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile) // Show line number on err.
	args, err := ParseFlags()
	if err != nil {
		log.Fatal(err)
	}
	mech.ChdirToHere()

	rootMP := gomods.ModulePath(args.ModRepo + args.RootModDirName)
	root, err := gomods.NewModuleTree(args.ModRepo, rootMP)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("The module dependency tree looks like this:")
	gomods.PrintModuleTree(0, root)

	fmt.Println("So based on that, the plan is this:")
	fmt.Printf(">>> Start plan <<<\n")

	// The leaves of this tree will be shed layer by layer.
	tree := eqnode.CopyTree(root)

planLoop:
	for {
		for _, leafMP := range gomods.LeafMPs(tree) {
			fmt.Printf("on module %v\n", leafMP.BaseDir())
			fmt.Printf("    update & commit changes\n")

			if len(tree.Kids) == 0 {
				break planLoop
			}

			for _, lu := range gomods.SortedLeafUsers(leafMP, tree) {
				fmt.Printf("    propagate its pseudo-version to %v's go.mod\n",
					lu.MP.BaseDir())
			}
		}

		eqnode.Shed(tree)
	}

	fmt.Println("Push all repositories to the remote.")
	fmt.Printf(">>> End plan <<<\n")
	fmt.Printf("Proceed and execute the plan? (y/n): ")
	reader := bufio.NewReader(os.Stdin)
	char, _, err := reader.ReadRune()
	if err != nil {
		log.Fatal(err)
	}
	if char != 'y' && char != 'Y' {
		return
	}

	// Replenish the tree.
	tree = eqnode.CopyTree(root)

	// Completes part of step 1.) for one of the leaf modules.
	updateAndTidy := func(leafMP gomods.ModulePath, goVersion string) {
		gomods.GoGet(leafMP)
		gomods.GoModTidy(leafMP)
		gomods.GoModEditGo(leafMP, goVersion)
		if changes, err := gomods.AssertDirtyState(leafMP); err != nil {
			log.Fatal(err)
		} else if len(changes) > 0 {
			if err = gomods.CommitChanges(leafMP, changes); err != nil {
				log.Fatal(err)
			}
			fmt.Printf("    ✅update & commit changes: %v\n", changes)
		} else {
			fmt.Printf("    ✅update & commit changes: no changes\n")
		}
	}

	// Completes part of step 3.) for one of the leaf modules.
	propagateNewVersion := func(
		leafMP gomods.ModulePath,
		ver string, // The new version of the given leaf module.
	) {
		once := &sync.Once{}
		for _, lu := range gomods.SortedLeafUsers(leafMP, tree) {

			var messages []string

			if !lu.Replaces(leafMP) {
				msg := fmt.Sprintf(
					"    ✅add replace directive to it in %v's go.mod\n",
					lu.MP.BaseDir())
				messages = append(messages, msg)
				err = lu.ModFile.AddReplace(
					leafMP.String(), "",
					"../"+filepath.Base(leafMP.BaseDir()), "",
				)
				if err != nil {
					log.Fatal(err)
				}
			}

			if verOk, err := lu.RequiresVersion(leafMP, ver); err != nil {
				log.Fatal(err)
			} else if !verOk {
				msg := fmt.Sprintf(
					"    ✅propagate its pseudo-version to %v's go.mod\n",
					lu.MP.BaseDir())
				messages = append(messages, msg)
				err = lu.ModFile.AddRequire(leafMP.String(), ver)
				if err != nil {
					log.Fatal(err)
				}
			}

			if len(messages) == 0 {
				continue
			}

			lu.ModFile.Cleanup()
			b, err := lu.ModFile.Format()
			if err != nil {
				log.Fatal(err)
			}
			err = os.WriteFile(lu.MP.FilePath(), b, 0664)
			if err != nil {
				log.Fatal(err)
			}
			once.Do(func() {
				fmt.Printf("    pseudo-version: %v\n", ver)
			})
			for _, msg := range messages {
				fmt.Print(msg)
			}
		}
	}

actionLoop:
	for {

		// Collect leaf node modules into a set of modules and apply steps 1.)
		// to 3.) for each module.
		for _, leafMP := range gomods.LeafMPs(tree) {
			fmt.Printf("on module %v\n", leafMP.BaseDir())

			// Step 1.) Update and tidy this leaf module and commit any changes.

			updateAndTidy(leafMP, args.GoVersion)

			// Step 2.) If the root module is the only node remaining on the
			// tree you are finished.

			if len(tree.Kids) == 0 {
				break actionLoop
			}

			// Step 3.) Propagate the new module version to the set of modules
			// who are its direct users.

			ver, err := leafMP.PseudoVersion()
			if err != nil {
				log.Fatal(err)
			}
			propagateNewVersion(leafMP, ver)
		}

		// Step 4.) Remove the leaves from the tree.

		eqnode.Shed(tree)

		// Step 5.) Go to step 1.)
	}

	for m := range eqnode.ContentSet(root) {
		gomods.PushChanges(m.MP)
	}
	fmt.Println("✅Push all repositories to the remote.")
}
