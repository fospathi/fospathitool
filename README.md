# App fospathitool

App fospathitool helps manage local development of the interrelated `"gitlab.com/fospathi/..."` Go modules.

## Development status

App fospathitool is unstable; expect breaking changes with every commit.

## License

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="https://licensebuttons.net/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://fospathi.inertialframe.space">
    <span property="dct:title">Christian Stewart</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">fospathitool</span>.
  This work is published from:
  <span property="vcard:Country" datatype="dct:ISO3166"
        content="GB" about="https://fospathi.inertialframe.space">
  United Kingdom</span>.
</p>
